import { Component, OnInit, ViewChild } from '@angular/core';
import { Category } from '../categories.service';
import { NgxMasonryComponent } from 'ngx-masonry';
import { IImage } from 'ng-simple-slideshow/src/app/modules/slideshow/IImage';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {
  @ViewChild(NgxMasonryComponent, { static: true }) masonry: any;

  imageUrls: (string | IImage)[] = [
    {
      url: 'assets/banner/AD_NevaLine_1920x800_4.jpg',
      backgroundSize: 'cover',
      backgroundPosition: 'bottom',
      href: '#'
    },
    {
      url: 'assets/banner/Eva_NevaLine_1920x800-03.jpg',
      backgroundSize: 'cover',
      backgroundPosition: 'bottom center',
      href: '#'
    },
    {
      url: 'assets/banner/Gosh_NevaLine_1920x800-02.jpg',
      backgroundSize: 'cover',
      backgroundPosition: 'bottom',
      href: '#'
    },
    {
      url: 'assets/banner/K-Beauty_NevaLine_1920x800-02.jpg',
      backgroundSize: 'cover',
      backgroundPosition: 'bottom',
      href: '#'
    },
    {
      url: 'assets/banner/LC_NevaLine_1920x800-05.jpg',
      backgroundSize: 'cover',
      backgroundPosition: 'bottom',
      href: '#'
    }
  ];

  categories: Category[] = [
    {
      id: 1,
      img: 'facenbody.jpg',
      height: '300px',
      wide: false,
      active: false,
      name: 'Уход за лицом и телом',
      brands: [
        'EVA ESTHETIC',
        'TEANA',
        'L\'ACTION',
        'SALLY HANSEN',
        'ADIDAS',
        'DORCO',
        'ARCHITECT DEMIDOFF',
        'MADES COSMETICS',
        'EASY SPA'
      ]
    },
    {
      id: 2,
      img: 'decorative.jpg',
      height: '300px',
      wide: false,
      active: false,
      name: 'Декоративная косметика',
      brands: [
        'GOSH',
        'EVA MOSAIC',
        'ГУРМАНДИЗ',
        'PUPA',
        'MAX FACTOR',
        'BOURJOIS',
        'RIMMEL',
        'PROFUSION',
        'L.O.C.K. COLOR'
      ]
    },
    {
      id: 3,
      img: 'korean.jpg',
      height: '200px',
      wide: true,
      active: false,
      name: 'Корейская косметика',
      brands: [
        'SNP',
        'JAYJUN',
        'MAXCLINIC',
        'WHEN',
        'PAKCARE',
        'L.O.C.K. COLOR',
        'MEDIHEAL'
      ]
    },
    {
      id: 4,
      img: 'countries.jpg',
      height: '300px',
      wide: false,
      active: false,
      name: 'Страны и бренды',
      brands: [
        'КОРЕЯ',
        'ДАНИЯ',
        'РОССИЯ',
        'ИТАЛИЯ',
        'ФРАНЦИЯ',
        'ГЕРМАНИЯ',
        'США',
        'НИДЕРЛАНДЫ',
        'ТАИЛАНД',
        'ВЕЛИКОБРИТАНИЯ'
      ]
    },
    {
      id: 5,
      img: 'perfume.jpg',
      height: '200px',
      wide: true,
      active: false,
      name: 'Парфюмерия',
      brands: [
        'DOLCE&GABBANA',
        'ISABEY',
        'JACQUES FATH',
        'MASAKI PARIS',
        'CHOPARD',
        'ICEBERG',
        'SERGIO TACCHINI',
        'MEXX',
        'BRUNO BANANI',
        'JAMES BOND',
        'LA FEE',
        'POLICE',
        'REPLAY'
      ]
    },
    {
      id: 6,
      img: 'acessories.jpg',
      height: '300px',
      wide: false,
      active: false,
      name: 'Аксессуары',
      brands: ['LEE STAFFORD', 'MADES HAIR CARE', 'GOSH HAIR CARE', 'JANEKE']
    }
  ];

  constructor() {}

  ngOnInit() {}

  catActivate(id: number, event: any) {
    // TODO: () => {} add callback
    const targetEl = event.target;

    const activeItem = this.categories.find(x => x.id === id);
    const prevActiveItem = this.categories.find(x => x.active === true);
    if (prevActiveItem) {
      prevActiveItem.active = false;
    }
    activeItem.active = !activeItem.active;
    // this.masonry.reloadItems();
    this.masonry.layout();
  }
}

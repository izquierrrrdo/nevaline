import { Injectable } from '@angular/core';

// Категория товаров
export class Category {
  id: number;
  name: string;
  height: string;
  wide: boolean;
  active: boolean;
  img: string;
  brands: Array<String>;
}

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  constructor() {}
}

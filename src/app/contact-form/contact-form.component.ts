import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent {
  submitted = false;

  contactForm = this.formBuilder.group({
    city: ['', Validators.required],
    name: ['', Validators.required],
    phone: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    message: ['', Validators.required],
    agreement: [true, Validators.requiredTrue]
  });

  get f() {
    return this.contactForm.controls;
  }

  constructor(private formBuilder: FormBuilder) {}

  onSubmit() {
    this.submitted = true;

    if (this.contactForm.invalid) {
      return;
    }

    console.log('submitted');
  }
}

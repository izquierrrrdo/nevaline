import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  expanded: boolean;
  private mobileMode: boolean;
  public innerWidth: any;
  constructor() {}

  ngOnInit() {
    if (window.innerWidth <= 768) {
      this.expanded = false;
      this.mobileMode = true;
    } else {
      this.expanded = true;
      this.mobileMode = false;
    }
  }

  closeMenu() {
    if (this.mobileMode) {
      this.expanded = false;
    }
  }
}

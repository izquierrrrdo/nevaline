import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NgxMasonryModule } from 'ngx-masonry';
import { SlideshowModule } from 'ng-simple-slideshow';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { CategoriesComponent } from './categories/categories.component';
import { BrandsComponent } from './brands/brands.component';
import { PartnersComponent } from './partners/partners.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { CountryComponent } from './country/country.component';

const appRoutes: Routes = [
  { path: '', component: CategoryListComponent },
  { path: 'categories', component: CategoriesComponent },
  { path: 'brands', component: BrandsComponent },
  { path: 'partners', component: PartnersComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'country', component: CountryComponent },
  { path: 'category/:id', component: PageNotFoundComponent },

  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CategoryListComponent,
    PageNotFoundComponent,
    FooterComponent,
    HeaderComponent,
    CategoriesComponent,
    BrandsComponent,
    PartnersComponent,
    ContactsComponent,
    ContactFormComponent,
    CountryComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes, { enableTracing: false }),
    BrowserModule,
    AppRoutingModule,
    NgxMasonryModule,
    SlideshowModule,
    ReactiveFormsModule
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule {}
